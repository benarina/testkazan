package com.worldline.training.java.client;

import org.junit.*;

import com.worldline.training.java.client.Gender;

import static org.junit.Assert.*;

public class GenderTest {

	@Test
	public void testGetValue()
		throws Exception {
		assertEquals("Mr", Gender.MALE.getText());
		assertEquals("Mrs", Gender.FEMALE.getText());
	}

}