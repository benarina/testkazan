package com.worldline.training.java.catalog;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;

import org.junit.Test;

import com.worldline.training.java.catalog.Catalog;
import com.worldline.training.java.catalog.Vehicle;
import com.worldline.training.java.catalog.engine.DieselEngine;

public class CatalogTest {

	@Test
	public void testCatalog_1()
		throws Exception {

		Catalog catalog = new Catalog();
		assertNotNull(catalog);
		assertEquals(0, catalog.getTotalValue());
	}

	@Test
	public void testAddVehicle_1()
		throws Exception {
		Catalog catalog = new Catalog();
		catalog.setVehicles(new HashSet<Vehicle>());
		assertEquals(0L, catalog.getTotalValue());
		
		catalog.addVehicle(new Vehicle(10L, "", "", new DieselEngine()));
		assertEquals(3010L, catalog.getTotalValue());
		Vehicle vehicle = new Vehicle(10L, "", "", new DieselEngine());
		catalog.addVehicle(vehicle);
		assertEquals(6020L, catalog.getTotalValue());
		catalog.removeVehicle(vehicle);
		assertEquals(3010L, catalog.getTotalValue());
	}

}