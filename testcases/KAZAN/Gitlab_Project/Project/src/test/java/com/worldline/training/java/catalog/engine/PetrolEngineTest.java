package com.worldline.training.java.catalog.engine;

import org.junit.*;

import com.worldline.training.java.catalog.engine.PetrolEngine;

import static org.junit.Assert.*;

public class PetrolEngineTest {

	@Test
	public void testDieselEngine()
		throws Exception {
		PetrolEngine petrol = new PetrolEngine();
		assertTrue(Engine.class.isInstance(petrol));
		assertEquals(2000, petrol.getPrice());
	}

}