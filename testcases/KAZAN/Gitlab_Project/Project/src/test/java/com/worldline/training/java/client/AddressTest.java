package com.worldline.training.java.client;

import org.junit.*;

import com.worldline.training.java.client.Address;

import static org.junit.Assert.*;

public class AddressTest {
	
	@Test
	public void testAddress()
		throws Exception {
		int number = 3;
		String street = "a";
		String town = "b";
		String country = "c";

		Address result = new Address(number, street, town, country);
		assertNotNull(result);
		assertEquals(3, result.getNumber());
		assertEquals("a", result.getStreet());
		assertEquals("b", result.getTown());
		assertEquals("c", result.getCountry());
		assertEquals("3 a b c", result.toString());
	}

}