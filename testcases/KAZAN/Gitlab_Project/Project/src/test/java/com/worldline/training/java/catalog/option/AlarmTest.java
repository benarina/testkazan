package com.worldline.training.java.catalog.option;

import org.junit.*;

import static org.junit.Assert.*;

public class AlarmTest {

	@Test
	public void testAlarm()
		throws Exception {
		Alarm alarm = new Alarm();

		assertTrue(Option.class.isInstance(alarm));
		assertEquals(2000, alarm.getPrice());
		assertEquals("Alarm", alarm.getDescription());
		assertEquals("alarm", alarm.getIdentifier());
	}

}