package com.worldline.training.java.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class FirmTest {

	@Test
	public void testFirm()
		throws Exception {
		Firm firm = new Firm("a", new Address(20,"c","d","e"), 120L);

		assertNotNull(firm);
		assertEquals("a", firm.getName());
		assertEquals(120L, firm.getBudget());
		assertEquals(null, firm.getRegistrationDate());
	}

}