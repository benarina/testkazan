/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.client;


/**
 * The Class address.
 */
public class Address {
	
	/** The number. */
	private int number;
	
	/** The street. */
	private String street;
	
	/** The town. */
	private String town;
	
	/** The country. */
	private String country;

	/**
	 * Instantiates a new address.
	 *
	 * @param number the number
	 * @param street the street
	 * @param town the town
	 * @param country the country
	 */
	public Address(int number, String street, String town, String country) {
		super();
		this.number = number;
		this.street = street;
		this.town = town;
		this.country = country;
	}

	/**
	 * Return value of attribute number.
	 *
	 * @return <code>int</code>
	 */
	public int getNumber() {
		return this.number;
	}

	/**
	 * Set value of attribute number.
	 *
	 * @param number            : (<code>int</code>) the new value of number
	 */
	public void setNumber(int number) {
		this.number = number;
	}

	/**
	 * Return value of attribute street.
	 *
	 * @return <code>String</code>
	 */
	public String getStreet() {
		return this.street;
	}

	/**
	 * Set value of attribute street.
	 *
	 * @param street            : (<code>String</code>) the new value of street
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Return value of attribute town.
	 *
	 * @return <code>String</code>
	 */
	public String getTown() {
		return this.town;
	}

	/**
	 * Set value of attribute town.
	 *
	 * @param town            : (<code>String</code>) the new value of town
	 */
	public void setTown(String town) {
		this.town = town;
	}

	/**
	 * Return value of attribute country.
	 *
	 * @return <code>String</code>
	 */
	public String getCountry() {
		return this.country;
	}

	/**
	 * Set value of attribute country.
	 *
	 * @param country            : (<code>String</code>) the new value of country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * To string.
	 *
	 * @return <code>String</code> :
	 */
	public String toString() {
		return this.number + " " + this.street + " " + this.town + " "
				+ this.country;
	}
}
