/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.client;

import java.util.List;
import java.util.Date;

import com.worldline.training.java.Shop;
import com.worldline.training.java.catalog.Vehicle;

/**
 * This class represent a Client of a {@link Shop}.
 */
public abstract class Client {
	
	/** The address. */
	protected Address address = null;
	
	/** The identifier. */
	protected int identifier;
	
	/** The number of instance. */
	protected static int numberOfInstance;
	
	/** The budget. */
	protected long budget;
	
	/** The registration date. */
	private Date registrationDate;

	/**
	 * Instantiates a new client.
	 */
	public Client() {
		super();
		numberOfInstance++;
	}

	/**
	 * Return value of attribute identifier.
	 *
	 * @return <code>Long</code>
	 */
	public int getIdentifier() {
		return this.identifier;
	}

	/**
	 * Set value of attribute identifier.
	 *
	 * @param identifier            : (<code>Long</code>) the new value of identifier
	 */
	public void setIdentifier(int identifier) {
		this.identifier = identifier;
	}

	/**
	 * Return value of attribute numberOfInstance.
	 *
	 * @return <code>int</code>
	 */
	public static int getNumberOfInstance() {
		return com.worldline.training.java.client.Client.numberOfInstance;
	}

	/**
	 * Set value of attribute numberOfInstance.
	 *
	 * @param numberOfInstance            : (<code>int</code>) the new value of numberOfInstance
	 */
	public static void setNumberOfInstance(int numberOfInstance) {
		com.worldline.training.java.client.Client.numberOfInstance = numberOfInstance;
	}

	/**
	 * Relation Set value of attribute address.
	 *
	 * @return the address
	 */
	public Address getAddress() {
		return this.address;
	}

	/**
	 * Relation Return value of attribute address.
	 *
	 * @param address the new address
	 * @return <code>address</code>
	 */
	public void setAddress(final Address address) {
		this.address = address;
	}

	/**
	 * Gets the full name.
	 *
	 * @return <code>String</code> :
	 */
	public abstract String getFullName();

	/**
	 * Set value of attribute budget.
	 *
	 * @param budget            : (<code>long</code>) the new value of budget
	 */
	public void setBudget(long budget) {
		this.budget = budget;
	}

	/**
	 * Return value of attribute budget.
	 *
	 * @return <code>long</code>
	 */
	public long getBudget() {
		return this.budget;
	}

	/**
	 * Select vehicle.
	 *
	 * @param vehicles            (<code>Collection</code>) :
	 * @return <code>Vehicle</code> :
	 */
	public abstract Vehicle selectVehicle(List<Vehicle> vehicles);

	/**
	 * Hash code.
	 *
	 * @return <code>int</code> :
	 */
	@Override
	public abstract int hashCode();

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public abstract boolean equals(Object obj);

	/**
	 * Set value of attribute registrationDate.
	 *
	 * @param registrationDate            : (<code>Date</code>) the new value of registrationDate
	 */
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	/**
	 * Return value of attribute registrationDate.
	 *
	 * @return <code>Date</code>
	 */
	public Date getRegistrationDate() {
		return this.registrationDate;
	}

	/**
	 * Bye.
	 */
	public void bye() {
		System.out.println("client: \t\tThank, See you...");
	}
}
