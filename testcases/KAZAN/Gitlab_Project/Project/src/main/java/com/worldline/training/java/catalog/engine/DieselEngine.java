/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.catalog.engine;


/**
 * The Class DieselEngine.
 */
public class DieselEngine extends Engine {
	/**
	 * {@inheritDoc}
	 */
	public long getPrice() {
		return 3000;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Diesel";
	}
}
