package com.worldline.training.java.client;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Client Factory is in charge of creating {@link Client} object depending of
 * String tokens array passed as an argument.
 */
public class ClientFactory {

	/**
	 * TODO: implement this method. The first token is the type of Client: Person or Firm
	 * 	- if the tokens represent a Person, call the method getPerson(String[] tokens)
	 * 	- if the tokens represent a Firm, call the method getFirm(String[] tokens)
	 * 
	 * Gets the client.
	 *
	 * @param tokens the tokens
	 * @return the client
	 */
	public static Client getClient(String[] tokens) {
		Client client = null;
		
		/*
		 * 	- if the tokens represent a Person, call the method getPerson(String[] tokens)
		 * 	- if the tokens represent a Firm, call the method getFirm(String[] tokens)
		 * 
		 */
		if (tokens[0].equalsIgnoreCase("Person")) {
			client = getPerson(tokens);
		} else {
			client = getFirm(tokens);
		}
		return client;
	}

	/**
	 * TODO: implement this method:
	 * 	- instanciate a Person using tokens values.
	 * 	- Be warning to Exception that can be thrown: {@link NumberFormatException} and {@link ParseException}
	 * 
	 * Gets the person.
	 *
	 * @param tokens the tokens
	 * @return the person
	 */
	private static Client getPerson(String[] tokens) {
		Person person = null;
		try {
			Address address = new Address(Integer.parseInt(tokens[4]), tokens[5],
					tokens[6], tokens[7]);
			Gender gender = Gender.valueOf(tokens[3]);
			long budget = Long.parseLong(tokens[8]);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date registrationDate = sdf.parse(tokens[9]);
			// Next lines must be the last ones because of Exception catching...!!!
			person = new Person(tokens[1], tokens[2], gender, address, budget);
			person.setRegistrationDate(registrationDate);
		} catch (NumberFormatException | ParseException e) {
			System.err.println("Error parsing client " + tokens[1] + " : "
					+ e.getMessage());
		}
		return person;
	}

	/**
	 * TODO: implement this method:
	 * 	- instanciate a Firm using tokens values.
	 * 	- Be warning to Exception that can be thrown: {@link NumberFormatException} and {@link ParseException}
	 * 
	 * Gets the firm.
	 *
	 * @param tokens the tokens
	 * @return the firm
	 */
	private static Client getFirm(String[] tokens) {
		Firm firm = null;
		/*
		 * 	- instanciate a Firm using tokens values.
		 * 	- Be warning to Exception that can be thrown: {@link NumberFormatException} and {@link ParseException}
		 */
		try {
			Address address = new Address(Integer.parseInt(tokens[2]), tokens[3],
					tokens[4], tokens[5]);
			long budget = Long.parseLong(tokens[6]);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date registrationDate = sdf.parse(tokens[7]);
			// Next lines must be the last ones because of Exception catching...!!!
			firm = new Firm(tokens[1], address, budget);
			firm.setRegistrationDate(registrationDate);
		} catch (NumberFormatException | ParseException e) {
			System.err.println("Error parsing client " + tokens[1] + " : "
					+ e.getMessage());
		}
		return firm;
	}
}
