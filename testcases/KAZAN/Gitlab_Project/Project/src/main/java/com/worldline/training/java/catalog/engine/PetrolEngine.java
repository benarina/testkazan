/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.catalog.engine;


/**
 * The Class PetrolEngine.
 */
public class PetrolEngine extends Engine {
	/**
	 * {@inheritDoc}
	 */
	public long getPrice() {
		return 2000;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Petrol";
	}
}
