package com.worldline.training.java.exception;

/**
 * The Class NoVehicleForBudgetException.
 */
public class NoVehicleForBudgetException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3085497077475418932L;

	/**
	 * Instantiates a new no vehicle for budget exception.
	 */
	public NoVehicleForBudgetException() {
	}

	/**
	 * Instantiates a new no vehicle for budget exception.
	 *
	 * @param message the message
	 */
	public NoVehicleForBudgetException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new no vehicle for budget exception.
	 *
	 * @param cause the cause
	 */
	public NoVehicleForBudgetException(Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new no vehicle for budget exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public NoVehicleForBudgetException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new no vehicle for budget exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 * @param enableSuppression the enable suppression
	 * @param writableStackTrace the writable stack trace
	 */
	public NoVehicleForBudgetException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
