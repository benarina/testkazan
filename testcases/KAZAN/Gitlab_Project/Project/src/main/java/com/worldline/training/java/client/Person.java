/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.client;

import java.util.List;

import com.worldline.training.java.catalog.Vehicle;

/**
 * The Class Person.
 */
public class Person extends Client {
	
	/** The first name. */
	private String firstName;
	
	/** The last name. */
	private String lastName;
	
	/** The gender. */
	private Gender gender;

	/**
	 * Instantiates a new person.
	 *
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param gender the gender
	 * @param address the address
	 * @param budget the budget
	 */
	public Person(String firstName, String lastName, Gender gender,
			Address address, long budget) {
		/*
		 * Call the constructor of Client class The attribut numberOfInstance
		 * will then been incremented
		 */
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.address = address;
		this.identifier = numberOfInstance;
		this.budget = budget;
	}

	/**
	 * Return value of attribute firstName.
	 *
	 * @return <code>String</code>
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * Set value of attribute firstName.
	 *
	 * @param firstName            : (<code>String</code>) the new value of firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Return value of attribute lastName.
	 *
	 * @return <code>String</code>
	 */
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * Set value of attribute lastName.
	 *
	 * @param lastName            : (<code>String</code>) the new value of lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Return value of attribute gender.
	 *
	 * @return <code>Gender</code>
	 */
	public Gender getGender() {
		return this.gender;
	}

	/**
	 * Set value of attribute gender.
	 *
	 * @param gender            : (<code>Gender</code>) the new value of gender
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	/**
	 * Gets the full name.
	 *
	 * @return <code>String</code> :
	 */
	public String getFullName() {
		return this.gender.getText() + " " + this.firstName + " "
				+ this.lastName + ". (client: " + this.identifier + ") ";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Vehicle selectVehicle(List<Vehicle> vehicles) {
		/*
		 */
		Vehicle result = null;
		for (Vehicle vehicle : vehicles) {
			if (result == null) {
				result = vehicle;
			} else {
				if (result.getOption().length < vehicle.getOption().length) {
					result = vehicle;
				}
			}
		}
		System.out
				.println("client: \t\tI choose this one: " + result.getDescription());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see com.worldline.training.java.Client#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (gender != other.gender)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		return true;
	}
}
