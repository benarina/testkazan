/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.client;

import java.util.List;

import com.worldline.training.java.catalog.Vehicle;
import com.worldline.training.java.catalog.engine.DieselEngine;

/**
 * The Class Firm.
 */
public class Firm extends Client {
	
	/** The name. */
	private String name;

	/**
	 * Instantiates a new firm.
	 *
	 * @param name the name
	 * @param address the address
	 * @param budget the budget
	 */
	public Firm(String name, Address address, long budget) {
		/*
		 * Call the constructor of Client class The attribut numberOfInstance
		 * will then been incremented
		 */
		super();
		this.name = name;
		this.address = address;
		this.budget = budget;
		this.identifier = numberOfInstance;
	}

	/**
	 * Return value of attribute name.
	 *
	 * @return <code>String</code>
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Set value of attribute name.
	 *
	 * @param name            : (<code>String</code>) the new value of name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the full name.
	 *
	 * @return <code>String</code> :
	 */
	public String getFullName() {
		return this.name + ". (client: " + this.identifier + ") ";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see com.worldline.training.java.Client#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Firm other = (Firm) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Vehicle selectVehicle(List<Vehicle> vehicles) {
		/*
		 */
		Vehicle result = null;
		for (Vehicle vehicle : vehicles) {
			if (vehicle.getEngine() instanceof DieselEngine) {
				result = vehicle;
			}
		}
		if (result == null && !vehicles.isEmpty()) {
			result = vehicles.get(0);
		}
		System.out
				.println("client: \t\tI choose this one: " + result.getDescription());
		return result;
	}
}
