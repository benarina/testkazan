/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.catalog.option;


/**
 * The Class MetallicPaint.
 */
public class MetallicPaint implements Option {
	/**
	 * {@inheritDoc}
	 */
	public long getPrice() {
		return 1000;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getDescription() {
		return "Metallic paint";
	}

	/**
	 * {@inheritDoc}
	 */
	public String getIdentifier() {
		return "metal";
	}
}
