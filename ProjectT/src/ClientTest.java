

import java.util.Date;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ClientTest</code> contains tests for the class <code>{@link Client}</code>.
 *
 * @generatedBy CodePro at 10/04/14 14:58
 * @author fr22240
 * @version $Revision: 1.0 $
 */
public class ClientTest {
	/**
	 * Run the void bye() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	@Test
	public void testBye_1()
		throws Exception {
		Firm fixture = new Firm("", new Address(1, "", "", ""), 1L);
		fixture.identifier = 1;

		fixture.bye();

		// add additional test code here
	}

	/**
	 * Run the Address getAdres() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	@Test
	public void testGetAdres_1()
		throws Exception {
		Firm fixture = new Firm("", new Address(1, "", "", ""), 1L);
		fixture.identifier = 1;

		Address result = fixture.getAdres();

		// add additional test code here
		assertNotNull(result);
		assertEquals("", result.getStreet());
		assertEquals("", result.getTown());
		assertEquals("1   ", result.toString());
		assertEquals(1, result.getNumber());
		assertEquals("", result.getCountry());
	}

	/**
	 * Run the long getBudget() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	@Test
	public void testGetBudget_1()
		throws Exception {
		Firm fixture = new Firm("", new Address(1, "", "", ""), 1L);
		fixture.identifier = 1;

		long result = fixture.getBudget();

		// add additional test code here
		assertEquals(1L, result);
	}

	/**
	 * Run the int getIdentifier() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	@Test
	public void testGetIdentifier_1()
		throws Exception {
		Firm fixture = new Firm("", new Address(1, "", "", ""), 1L);
		fixture.identifier = 1;

		int result = fixture.getIdentifier();

		// add additional test code here
		assertEquals(1, result);
	}

	/**
	 * Run the int getNumberOfInstance() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	@Test
	public void testGetNumberOfInstance_1()
		throws Exception {

		int result = Client.getNumberOfInstance();

		// add additional test code here
		assertEquals(2, result);
	}

	/**
	 * Run the Date getRegistrationDate() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	@Test
	public void testGetRegistrationDate_1()
		throws Exception {
		Firm fixture = new Firm("", new Address(1, "", "", ""), 1L);
		fixture.identifier = 1;

		Date result = fixture.getRegistrationDate();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the void setAdres(Address) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	@Test
	public void testSetAdres_1()
		throws Exception {
		Firm fixture = new Firm("", new Address(1, "", "", ""), 1L);
		fixture.identifier = 1;
		Address address = new Address(1, "", "", "");

		fixture.setAddress(address);

		// add additional test code here
	}

	/**
	 * Run the void setBudget(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	@Test
	public void testSetBudget_1()
		throws Exception {
		Firm fixture = new Firm("", new Address(1, "", "", ""), 1L);
		fixture.identifier = 1;
		long budget = 1L;

		fixture.setBudget(budget);

		// add additional test code here
	}

	/**
	 * Run the void setIdentifier(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	@Test
	public void testSetIdentifier_1()
		throws Exception {
		Firm fixture = new Firm("", new Address(1, "", "", ""), 1L);
		fixture.identifier = 1;
		int identifier = 1;

		fixture.setIdentifier(identifier);

		// add additional test code here
	}

	/**
	 * Run the void setNumberOfInstance(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	@Test
	public void testSetNumberOfInstance_1()
		throws Exception {
		int numberOfInstance = 1;

		Client.setNumberOfInstance(numberOfInstance);

		// add additional test code here
	}

	/**
	 * Run the void setRegistrationDate(Date) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	@Test
	public void testSetRegistrationDate_1()
		throws Exception {
		Firm fixture = new Firm("", new Address(1, "", "", ""), 1L);
		fixture.identifier = 1;
		Date registrationDate = new Date();

		fixture.setRegistrationDate(registrationDate);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/04/14 14:58
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ClientTest.class);
	}
}