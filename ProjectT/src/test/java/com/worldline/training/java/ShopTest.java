package com.worldline.training.java;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.worldline.training.java.client.Address;
import com.worldline.training.java.client.Firm;
import com.worldline.training.java.client.Gender;
import com.worldline.training.java.client.Person;

public class ShopTest {
	@Test
	public void testShop()
		throws Exception {

		Shop shop = new Shop();
		shop.setVendor(new Vendor("vendor",0));
		assertNotNull(shop);
		assertNotNull(shop.getCatalog());
		assertEquals(0, shop.getCatalog().getTotalValue());
		assertNotNull(shop.getClients());
		assertEquals(0, shop.getClients().size());
		
		shop.enterClient(new Person("a","b",Gender.MALE,new Address(12, "c", "d", "e"), 100001));
		assertEquals(1, shop.getClients().size());
		shop.enterClient(new Firm("a", new Address(12, "a", "b", "c"), 100001));
		assertEquals(2, shop.getClients().size());
	}

}