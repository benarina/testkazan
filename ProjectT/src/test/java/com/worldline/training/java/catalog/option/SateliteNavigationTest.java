package com.worldline.training.java.catalog.option;

import org.junit.*;

import static org.junit.Assert.*;

public class SateliteNavigationTest {

	@Test
	public void testSatelliteNavigation()
		throws Exception {
		SateliteNavigation satelite = new SateliteNavigation();

		assertTrue(Option.class.isInstance(satelite));
		assertEquals("Navigation system", satelite.getDescription());
		assertEquals("satelite", satelite.getIdentifier());
		assertEquals(1500, satelite.getPrice());
	}

}