package com.worldline.training.java.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class PersonTest {

	@Test
	public void testPerson_1()
		throws Exception {
		Person person = new Person("a", "b", Gender.FEMALE, new Address(20,"c","d","e"), 120L);

		assertNotNull(person);
		assertEquals("a", person.getFirstName());
		assertEquals("b", person.getLastName());
		assertEquals(120L, person.getBudget());
		assertEquals(null, person.getRegistrationDate());
	}

}