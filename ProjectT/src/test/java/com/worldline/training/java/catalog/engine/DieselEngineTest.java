package com.worldline.training.java.catalog.engine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class DieselEngineTest {

	@Test
	public void testDieselEngine()
		throws Exception {
		DieselEngine diesel = new DieselEngine();
		assertTrue(Engine.class.isInstance(diesel));
		assertEquals(3000, diesel.getPrice());
	}

}