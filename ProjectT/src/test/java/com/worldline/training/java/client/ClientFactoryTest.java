package com.worldline.training.java.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ClientFactoryTest {

	@Test
	public void testGetClientPerson()
		throws Exception {
		String[] tokens = new String[] {
				"Person","a","b","MALE","12","c","d","e","20000","2013-12-10 10:12:00"
		};

		Client client = ClientFactory.getClient(tokens);
		assertNotNull(client);
		assertTrue(Person.class.isInstance(client));
		assertEquals("a",((Person)client).getFirstName());
		assertEquals("b",((Person)client).getLastName());
		assertEquals(Gender.MALE,((Person)client).getGender());
		assertNotNull(client.getAddress());
		assertEquals("12 c d e",client.getAddress().toString());
	}

	@Test
	public void testGetClientFirm()
		throws Exception {
		String[] tokens = new String[] {
				"Firm","a","13","b","c","d","25000","2008-03-10 10:12:00"
		};

		Client client = ClientFactory.getClient(tokens);
		assertNotNull(client);
		assertTrue(Firm.class.isInstance(client));
		assertEquals("a",((Firm)client).getName());
		
		assertNotNull(client.getAddress());
		assertEquals("13 b c d",client.getAddress().toString());
	}
}