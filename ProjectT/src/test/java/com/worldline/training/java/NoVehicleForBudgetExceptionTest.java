package com.worldline.training.java;

import org.junit.*;

import com.worldline.training.java.exception.NoVehicleForBudgetException;

import static org.junit.Assert.*;

public class NoVehicleForBudgetExceptionTest {

	@Test
	public void testNoVehicleForBudgetException()
		throws Exception {

		NoVehicleForBudgetException  exception = new NoVehicleForBudgetException();
		assertNotNull(exception);
		assertEquals("com.worldline.training.java.exception.NoVehicleForBudgetException", exception.toString());
		assertEquals(null, exception.getCause());
		assertEquals(null, exception.getMessage());
		assertEquals(null, exception.getLocalizedMessage());
	}

	@Test
	public void testNoVehicleForBudgetExceptionWithMessage()
		throws Exception {
		String message = "a message";

		NoVehicleForBudgetException exception = new NoVehicleForBudgetException(message);
		assertNotNull(exception);
		assertEquals("com.worldline.training.java.exception.NoVehicleForBudgetException: a message", exception.toString());
		assertEquals(null, exception.getCause());
		assertEquals("a message", exception.getMessage());
		assertEquals("a message", exception.getLocalizedMessage());
	}

	@Test
	public void testNoVehicleForBudgetExceptionWithThrowable()
		throws Exception {
		Throwable cause = new Throwable();

		NoVehicleForBudgetException exception = new NoVehicleForBudgetException(cause);
		assertNotNull(exception);
		assertEquals("com.worldline.training.java.exception.NoVehicleForBudgetException: java.lang.Throwable", exception.toString());
		assertEquals("java.lang.Throwable", exception.getMessage());
		assertEquals("java.lang.Throwable", exception.getLocalizedMessage());
	}

	@Test
	public void testNoVehicleForBudgetExceptionWithMessageWithThrowable()
		throws Exception {
		String message = "a message";
		Throwable cause = new Throwable();

		NoVehicleForBudgetException exception = new NoVehicleForBudgetException(message, cause);
		assertNotNull(exception);
		assertEquals("com.worldline.training.java.exception.NoVehicleForBudgetException: a message", exception.toString());
		assertEquals("a message", exception.getMessage());
		assertEquals("a message", exception.getLocalizedMessage());
	}

}