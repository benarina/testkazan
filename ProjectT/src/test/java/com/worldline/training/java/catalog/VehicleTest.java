package com.worldline.training.java.catalog;

import org.junit.*;

import com.worldline.training.java.catalog.engine.DieselEngine;
import com.worldline.training.java.catalog.engine.Engine;
import com.worldline.training.java.catalog.engine.PetrolEngine;

import static org.junit.Assert.*;

public class VehicleTest {

	@Test
	public void testVehicleDiesel()
		throws Exception {

		long price = 15000;
		String brand = "brand";
		String model = "model";
		Engine engine = new DieselEngine();

		Vehicle vehicle = new Vehicle(price, brand, model, engine);
		assertNotNull(vehicle);
		assertEquals(15000, vehicle.getPrice());
		assertEquals(18000, vehicle.getTotalPrice());
		assertEquals("brand", vehicle.getBrand());
		assertEquals("model", vehicle.getModel());
		assertEquals("brand model (Diesel) : 18000", vehicle.getDescription());
		assertTrue(DieselEngine.class.isInstance(vehicle.getEngine()));
		assertEquals(0, vehicle.getOption().length);
	}


	@Test
	public void testVehiclePetrol()
		throws Exception {

		long price = 15000;
		String brand = "brand";
		String model = "model";
		Engine engine = new PetrolEngine();

		Vehicle vehicle = new Vehicle(price, brand, model, engine);
		assertNotNull(vehicle);
		assertEquals(15000, vehicle.getPrice());
		assertEquals(17000, vehicle.getTotalPrice());
		assertEquals("brand", vehicle.getBrand());
		assertEquals("model", vehicle.getModel());
		assertEquals("brand model (Petrol) : 17000", vehicle.getDescription());
		assertTrue(PetrolEngine.class.isInstance(vehicle.getEngine()));
		assertEquals(0, vehicle.getOption().length);
	}

}