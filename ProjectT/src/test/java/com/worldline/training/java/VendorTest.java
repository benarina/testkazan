package com.worldline.training.java;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.worldline.training.java.catalog.Catalog;
import com.worldline.training.java.catalog.Vehicle;
import com.worldline.training.java.catalog.engine.DieselEngine;
import com.worldline.training.java.exception.NoMoreVehicleInCatalogException;
import com.worldline.training.java.exception.NoVehicleForBudgetException;

public class VendorTest {

	@Test
	public void testVendor()
		throws Exception {

		Vendor result = new Vendor("a", 0);
		assertNotNull(result);
		assertEquals(0L, result.getSales());
		assertEquals("a", result.getName());
		assertEquals(null, result.getIdentifier());
	}
		
	@Test(expected=NoMoreVehicleInCatalogException.class)
	public void testGetAvailableVehiculesForBudgetNoMoreVehicleInCatalogException() throws NoVehicleForBudgetException, NoMoreVehicleInCatalogException {
		Vendor result = new Vendor("a", 0);
		Catalog catalog = new Catalog();
		catalog.getVehicles().clear();
		result.getAvailableVehiculesForBudget(catalog, 100000);
		fail("getAvailableVehiculesForBudget should throw NoVehicleForBudgetException");
	}		

	
	@Test(expected=NoVehicleForBudgetException.class)
	public void testGetAvailableVehiculesForBudgetNoVehicleForBudgetException() throws NoVehicleForBudgetException, NoMoreVehicleInCatalogException {
		Catalog catalog = new Catalog();
		catalog.getVehicles().add(new Vehicle(10, "brand", "model",
				new DieselEngine()));
		new Vendor("a", 0).getAvailableVehiculesForBudget(catalog, 10);
		fail("getAvailableVehiculesForBudget should throw NoVehicleForBudgetException");
	}		
}