package com.worldline.training.java.catalog.option;

import org.junit.*;

import static org.junit.Assert.*;

public class MetallicPaintTest {

	@Test
	public void testMetallicPaint()
		throws Exception {
		MetallicPaint paint = new MetallicPaint();

		assertTrue(Option.class.isInstance(paint));
		assertEquals("Metallic paint", paint.getDescription());
		assertEquals("metal", paint.getIdentifier());
		assertEquals(1000, paint.getPrice());
	}

}