/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.client;


/**
 * The Enum Gender.
 */
public enum Gender {
	
	/** The male. */
	MALE("Mr"),
	
	/** The female. */
	FEMALE("Mrs");
	
	/** The text. */
	private String text;

	/**
	 * Instantiates a new gender.
	 *
	 * @param text the text
	 */
	private Gender(String text) {
		this.text = text;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getText() {
		return this.text;
	}
}
