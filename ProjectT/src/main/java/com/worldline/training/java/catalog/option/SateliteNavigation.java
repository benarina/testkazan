/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.catalog.option;


/**
 * The Class SateliteNavigation.
 */
public class SateliteNavigation implements Option {
	/**
	 * {@inheritDoc}
	 */
	public long getPrice() {
		return 1500;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getDescription() {
		return "Navigation system";
	}

	/**
	 * {@inheritDoc}
	 */
	public String getIdentifier() {
		return "satelite";
	}
}
