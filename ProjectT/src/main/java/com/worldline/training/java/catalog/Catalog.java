/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.catalog;

import java.util.HashSet;
import java.util.Set;

/**
 * The Class Catalog.
 */
public class Catalog {
	
	/** The vehicles. */
	private Set<Vehicle> vehicles = new HashSet<Vehicle>();

	/**
	 * Instantiates a new catalog.
	 */
	public Catalog() {
		super();
	}

	/**
	 * Relation Set value of attribute vehicle.
	 *
	 * @return the vehicles
	 */
	public Set<Vehicle> getVehicles() {
		return this.vehicles;
	}

	/**
	 * Relation Return value of attribute vehicle.
	 *
	 * @param vehicles the new vehicles
	 * @return <code>Vehicle</code>
	 */
	public void setVehicles(Set<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	/**
	 * Gets the total value.
	 *
	 * @return <code>long</code> :
	 */
	public long getTotalValue() {
		long result = 0;
		for (Vehicle vehicle : this.vehicles) {
			result += vehicle.getTotalPrice();
		}
		return result;
	}

	/**
	 * Remove method concerning association.
	 *
	 * @param vehicle the vehicle
	 * @return the catalog
	 */
	public Catalog removeVehicle(final Vehicle vehicle) {
		this.vehicles.remove(vehicle);
		return this;
	}

	/**
	 * Add method concerning association.
	 *
	 * @param vehicle the vehicle
	 * @return the catalog
	 */
	public Catalog addVehicle(final Vehicle vehicle) {
		this.vehicles.add(vehicle);
		return this;
	}
}
