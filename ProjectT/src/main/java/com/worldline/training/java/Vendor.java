/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;

import com.worldline.training.java.catalog.Catalog;
import com.worldline.training.java.catalog.Vehicle;
import com.worldline.training.java.client.Client;
import com.worldline.training.java.client.Firm;
import com.worldline.training.java.exception.NoMoreVehicleInCatalogException;
import com.worldline.training.java.exception.NoVehicleForBudgetException;

/**
 * The Class Vendor.
 */
public class Vendor {
	
	/** The identifier. */
	private String identifier;
	
	/** The name. */
	private String name;
	
	/** The sales. */
	private long sales;

	/**
	 * Instantiates a new vendor.
	 *
	 * @param name the name
	 * @param sales the sales
	 */
	public Vendor(String name, long sales) {
		super();
		this.name = name;
		this.sales = sales;
	}

	/**
	 * Return value of attribute identifier.
	 *
	 * @return <code>String</code>
	 */
	public String getIdentifier() {
		return this.identifier;
	}

	/**
	 * Set value of attribute identifier.
	 *
	 * @param identifier            : (<code>String</code>) the new value of identifier
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * Return value of attribute name.
	 *
	 * @return <code>String</code>
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Set value of attribute name.
	 *
	 * @param name            : (<code>String</code>) the new value of name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Return value of attribute sales.
	 *
	 * @return <code>long</code>
	 */
	public long getSales() {
		return this.sales;
	}

	/**
	 * Set value of attribute sales.
	 *
	 * @param sales            : (<code>long</code>) the new value of sales
	 */
	public void setSales(long sales) {
		this.sales = sales;
	}

	/**
	 * Welcome.
	 *
	 * @param client            (<code>Client</code>) :
	 */
	public void welcome(Client client) {
		if(client instanceof Firm && ((Firm)client).getName().equals("SA Demolition")) {
			System.out.println("vendor: Welcome back "+client.getFullName()+"!!!");
		} else {
			System.out.println("vendor: Welcome "+client.getFullName()+"!");
		}
		Date registrationDate = client.getRegistrationDate();
		if(registrationDate != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.YEAR, -1);
			if(registrationDate.before(calendar.getTime())) {
				System.out.println("vendor: I hope this new catalog will enjoy you !");
			}
		}
	}

	/**
	 * Show catalog elements.
	 *
	 * @param catalog            (<code>Catalog</code>) :
	 * @throws NoMoreVehicleInCatalogException 
	 */
	public void showCatalog(Catalog catalog) throws NoMoreVehicleInCatalogException {
		System.out.println("Available vehicles in Shop catalog:");
		if(catalog.getVehicles().isEmpty()) {
			throw new NoMoreVehicleInCatalogException("No more Vehicle in Catalog...");
		}
		for (Vehicle vehicle : catalog.getVehicles()) {
			System.out.println("\t" + vehicle.getDescription());
		}
	}

	/**
	 * 
	 * Present the available vehicles in catalog for a specific budget.
	 *
	 * @param catalog            (<code>Catalog</code>) :
	 * @param budget            (<code>long</code>) :
	 * @throws NoVehicleForBudgetException the no vehicle for budget exception
	 * @return <code>Vehicle</code> :
	 * @throws NoMoreVehicleInCatalogException 
	 */
	public Collection<Vehicle> getAvailableVehiculesForBudget(Catalog catalog, long budget) throws NoVehicleForBudgetException, NoMoreVehicleInCatalogException {
		Set<Vehicle> proposedVehicles = new HashSet<>();
		if(catalog.getVehicles().isEmpty()) {
			throw new NoMoreVehicleInCatalogException("vendor: Sorry, but no more Vehicle in Catalog...");
		}
		for (Vehicle vehicle : catalog.getVehicles()) {
			if (vehicle.getTotalPrice() <= budget) {
				System.out.println("vendor: \t" + vehicle.getDescription());
				proposedVehicles.add(vehicle);
			}
		}
		if (proposedVehicles.isEmpty()) {
			throw new NoVehicleForBudgetException("vendor: \tSorry but i don't have any vehicle for your budget...");
		}
		return proposedVehicles;
	}

	/**
	 * Sell.
	 *
	 * @param client            (<code>Client</code>) :
	 * @param vehicle            (<code>Vehicle</code>) :
	 * @param catalog            (<code>Catalog</code>) :
	 * @param sales            (<code>Map</code>) :
	 */
	public void sell(Client client, Vehicle vehicle, Catalog catalog,
			Map<Client, Vehicle> sales) {
		// remove the vehicle from the catalog
		catalog.removeVehicle(vehicle);
		// decrease the budget of the client with the total value of the vehicle
		client.setBudget(client.getBudget() - vehicle.getTotalPrice());
		// increase its sales with the total value of the vehicle
		this.sales += vehicle.getTotalPrice();
		// add an entry the the sales attribute for this client and the choiced
		// vehicle.
		sales.put(client, vehicle);
	}
}
