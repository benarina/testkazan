/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import com.worldline.training.java.catalog.Catalog;
import com.worldline.training.java.catalog.Vehicle;
import com.worldline.training.java.catalog.engine.DieselEngine;
import com.worldline.training.java.catalog.engine.PetrolEngine;
import com.worldline.training.java.catalog.option.Alarm;
import com.worldline.training.java.catalog.option.MetallicPaint;
import com.worldline.training.java.catalog.option.SateliteNavigation;
import com.worldline.training.java.client.Client;
import com.worldline.training.java.client.ClientFactory;
import com.worldline.training.java.exception.NoMoreVehicleInCatalogException;
import com.worldline.training.java.exception.NoVehicleForBudgetException;


/**
 * Shop is the main entry point for this practical work.
 * A shop contains a list of {@link Client}, a {@link Catalog} of {@link Vehicle} and a {@link Vendor}.
 * Shop is the central point for {@link Vehicle} sales to {@link Client} by the {@link Vendor}. 
 */
public class Shop {

	/**
	 * The {@link Vendor} of the Shop.
	 */
	private Vendor vendor = null;

	/**
	 * The {@link Catalog} of the Shop.
	 */
	public Catalog catalog = null;

	/**
	 * sales is a {@link Map} regrouping the {@link Vehicle} sales for {@link Client} by a {@link Vendor}.
	 */
	private Map<Client, Vehicle> sales;

	/**
	 * The list of known {@link Client} of the Shop.
	 */
	private Set<Client> clients = new HashSet<Client>();

	/**
	 * Default constructor for the Shop class.
	 */
	public Shop() {
		// Call the parent constructor.
		super();
		// Initialize the sales attribute
		this.sales = new HashMap<>();
		// Create an instance for the Catalog of this Shop
		this.catalog = new Catalog();
	}


	/**
	 * Getter method for attribute {@link Shop#vendor}.
	 *
	 * @return {@link Vendor#} instance
	 */
	public Vendor getVendor() {
		return this.vendor;
	}


	/**
	 * Setter method for attribute {@link Shop#vendor}.
	 *
	 * @param vendor the new vendor
	 */
	public void setVendor(final Vendor vendor) {
		this.vendor = vendor;
	}

	/**
	 * Getter method for attribute {@link Shop#catalog}.
	 *
	 * @return {@link Shop#catalog} instance
	 */
	public Catalog getCatalog() {
		return this.catalog;
	}


	/**
	 * Setter method for attribute {@link Shop#catalog}.
	 *
	 * @param catalog the new catalog
	 */
	public void setCatalog(final Catalog catalog) {
		this.catalog = catalog;
	}


	/**
	 * This method simulate a {@link Client} entering the {@link Shop}
	 * When a {@link Client} enter the {@link Shop}, he's welcomed by the {@link Vendor}. 
	 * Then, the {@link Vendor} present to the {@link Client} the {@link Catalog} of {@link Vehicle} available for sale.
	 * If the {@link Client} find a {@link Vehicle} corresponding with he's budget, he buy it.
	 * Then, the {@link Client} go out saying bye... 
	 * 
	 * @param client The {@link Client} instance entering the {@link Shop}
	 */
	public void enterClient(Client client) {
		System.out.println("\nClient enter in the Shop:");
		this.clients.add(client);
		this.vendor.welcome(client);
		try {
			Collection<Vehicle> vehicles = this.vendor.getAvailableVehiculesForBudget(this.catalog,
					client.getBudget());
			if (!vehicles.isEmpty()) {
				List<Vehicle> forChoice = new ArrayList<>();
				forChoice.addAll(vehicles);
				Vehicle choice = client.selectVehicle(forChoice);
				this.vendor.sell(client, choice, this.catalog, this.sales);
			}
		} catch (NoVehicleForBudgetException | NoMoreVehicleInCatalogException e) {
			System.out.println("\t"+e);
		} finally {
			client.bye();
		}
		System.out.println();
	}


	/**
	 * Setter method for the {@link Shop#sales} attribute.
	 *
	 * @param sales the sales
	 */
	public void setSales(Map<Client, Vehicle> sales) {
		this.sales = sales;
	}


	/**
	 * Getter method for the {@link Shop#sales} attribute.
	 *
	 * @return the sales
	 */
	public Map<Client, Vehicle> getSales() {
		return this.sales;
	}


	/**
	 * Main method of the Shop.
	 * This method is the entry point for this practical work validation.
	 *
	 * @param args the arguments
	 * @throws Exception the exception
	 */
	public static void main(String[] args) throws Exception {
		// Initialize the Shop
		Shop shop = new Shop();
		
		// Initialize the Vendor of the Shop
		shop.setVendor(new Vendor("Ben", 0));
		
		// Initialize the Shop Catalog
		shop.getCatalog().getVehicles().add(new Vehicle(17000, "Peugeot", "308", new DieselEngine(),
				new MetallicPaint()));
		shop.getCatalog().getVehicles().add(new Vehicle(15000, "Peugeot", "208", new PetrolEngine()));
		shop.getCatalog().getVehicles().add(new Vehicle(30000, "Peugeot", "RCZ", new PetrolEngine(),
				new MetallicPaint(), new Alarm(), new SateliteNavigation()));
		shop.getCatalog().getVehicles().add(new Vehicle(20000, "Renault", "Megane", new DieselEngine(),
				new SateliteNavigation()));
		shop.getCatalog().getVehicles().add(new Vehicle(15000, "Renault", "Clio", new PetrolEngine()));
		
		// The vendor present catalog of Vehicule available for sale 
		shop.getVendor().showCatalog(shop.getCatalog());
		System.out.println();
		
		// Print the total value of the Catalog
		System.out.println("\nTotal value of the Catalog: "
				+ shop.getCatalog().getTotalValue());
		System.out.println();
		
		// Load a client list from a file to simulate a list of client entering the Shop
		List<Client> clients = shop.loadClients("src/main/resources/clients.txt");
		for (Client client : clients) {
			// For each Client found in the file, we simulate an entrance in the Shop
			shop.enterClient(client);
			System.out.println();
		}
		
		// Print the total value of the Catalog after all sales
		System.out.println("\nTotal value of the Catalog: "
				+ shop.getCatalog().getTotalValue());
		
		// Print all sales
		System.out.println("Total sales: " + shop.getVendor().getSales());
		
		// Print all known clients
		shop.showKnownClients();
	}


	/**
	 * Print the list of all known Clients by the Shop
	 * The registration date of the Client is printed in the NewYork {@link TimeZone} and in the Paris {@link TimeZone}.
	 */
	private void showKnownClients() {
		System.out.println("\nKnown clients:");
		for (Client client : this.clients) {
			Date registrationDate = client.getRegistrationDate();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("America/New_York"));
			String dateNewYork=sdf.format(registrationDate);
			sdf.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
			String dateParis=sdf.format(registrationDate);
			System.out.println("\t" + client.getFullName() + "\n\tregistration date: \n\t\tParis  -"+dateParis+" \n\t\tNewYork-"+dateNewYork);
		}
	}


	/**
	 * Setter method for the attribute {@link Shop#clients}.
	 *
	 * @param clients the new clients
	 */
	public void setClients(final Set<Client> clients) {
		this.clients = clients;
	}


	/**
	 * Getter method for the attribute {@link Shop#clients}.
	 *
	 * @return the clients
	 */
	public Set<Client> getClients() {
		return java.util.Collections.unmodifiableSet(clients);
	}


	/**
	 * TODO: Implement this method:
	 * 	- Open and read the file with file name passed as a parameter
	 * 	- For each line of the file:
	 * 		- split the line in an array of {@link String}: tokens
	 * 		- call the method {@link ClientFactory#getClient(String[])} with tokens as a parameter
	 * 		- add the returned {@link Client} to the clients variable. 
	 *  - You can use {@link File} {@link FileReader}, {@link BufferedReader}.
	 *  - Be Warning with Exceptions that may be thrown...
	 * 
	 * Initialize the {@link Client} list from a data file.
	 *
	 * @param fileName the data file name to use to initialize a {@link Client} list
	 * @return a {@link List}<{@link Client}>  initialized from a data file
	 */
	private List<Client> loadClients(String fileName) {
		List<Client> clients = new ArrayList<>();
		
		FileReader reader = null;
		BufferedReader bufferedReader = null;
		try {
			reader = new FileReader(new File(fileName));
			bufferedReader = new BufferedReader(reader);
			
			while (bufferedReader.ready()) {
				String line = bufferedReader.readLine();
				String[] tokens = line.split(";");
				if(tokens!=null && tokens.length>0) {
					Client client = ClientFactory.getClient(tokens);
					if (client!= null) {
						clients.add(client);
					}
				}
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					System.err.println("Error occured during buffer close...");
				}
			}
			if(reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// Do nothing
				}
			}
		}
		
		return clients;
	}

}
