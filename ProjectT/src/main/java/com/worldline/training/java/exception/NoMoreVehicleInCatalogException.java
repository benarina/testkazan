package com.worldline.training.java.exception;

/**
 * The Class NoMoreVehiclesInCatalogException.
 */
public class NoMoreVehicleInCatalogException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7801162350539197340L;

	/**
	 * Instantiates a new no more vehicle exception.
	 */
	public NoMoreVehicleInCatalogException() {
	}

	/**
	 * Instantiates a new no more vehicle exception.
	 *
	 * @param message the message
	 */
	public NoMoreVehicleInCatalogException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new no more vehicle exception.
	 *
	 * @param cause the cause
	 */
	public NoMoreVehicleInCatalogException(Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new no more vehicle exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public NoMoreVehicleInCatalogException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new no more vehicle exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 * @param enableSuppression the enable suppression
	 * @param writableStackTrace the writable stack trace
	 */
	public NoMoreVehicleInCatalogException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
