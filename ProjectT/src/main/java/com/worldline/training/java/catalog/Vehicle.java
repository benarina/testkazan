/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.catalog;

import com.worldline.training.java.catalog.engine.Engine;
import com.worldline.training.java.catalog.option.Option;


/**
 * The Class Vehicle.
 */
public class Vehicle {
	
	/** The engine. */
	private Engine engine = null;
	
	/** The price. */
	private long price;
	
	/** The brand. */
	private String brand;
	
	/** The model. */
	private String model;
	
	/** The option. */
	private Option[] option = new Option[] {};

	/**
	 * Instantiates a new vehicle.
	 *
	 * @param price the price
	 * @param brand the brand
	 * @param model the model
	 * @param engine the engine
	 */
	public Vehicle(long price, String brand, String model, Engine engine) {
		this.price = price;
		this.brand = brand;
		this.model = model;
		this.engine = engine;
	}

	/**
	 * Instantiates a new vehicle.
	 *
	 * @param price the price
	 * @param brand the brand
	 * @param model the model
	 * @param engine the engine
	 * @param options the options
	 */
	public Vehicle(long price, String brand, String model, Engine engine,
			Option... options) {
		super();
		this.price = price;
		this.brand = brand;
		this.model = model;
		this.option = options;
		this.engine = engine;
	}

	/**
	 * Return value of attribute price.
	 *
	 * @return <code>long</code>
	 */
	public long getPrice() {
		return this.price;
	}

	/**
	 * Set value of attribute price.
	 *
	 * @param price            : (<code>long</code>) the new value of price
	 */
	public void setPrice(long price) {
		this.price = price;
	}

	/**
	 * Return value of attribute brand.
	 *
	 * @return <code>String</code>
	 */
	public String getBrand() {
		return this.brand;
	}

	/**
	 * Set value of attribute brand.
	 *
	 * @param brand            : (<code>String</code>) the new value of brand
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * Return value of attribute model.
	 *
	 * @return <code>String</code>
	 */
	public String getModel() {
		return this.model;
	}

	/**
	 * Set value of attribute model.
	 *
	 * @param model            : (<code>String</code>) the new value of model
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * Relation Set value of attribute engine.
	 *
	 * @return the engine
	 */
	public Engine getEngine() {
		return this.engine;
	}

	/**
	 * Relation Return value of attribute engine.
	 *
	 * @param engine the new engine
	 * @return <code>Engine</code>
	 */
	public void setEngine(final Engine engine) {
		this.engine = engine;
	}

	/**
	 * Gets the total price.
	 *
	 * @return <code>long</code> :
	 */
	public long getTotalPrice() {
		long resultPrice = this.price + this.getEngine().getPrice();
		for (Option option : this.option) {
			resultPrice += option.getPrice();
		}
		return resultPrice;
	}

	/**
	 * Gets the description.
	 *
	 * @return <code>String</code> :
	 */
	public String getDescription() {
		String result = this.brand + " " + this.model + " (" + engine + ")";
		for (Option option : this.option) {
			result += "-" + option.getDescription();
		}
		result += " : " + getTotalPrice();
		return result;
	}

	/**
	 * Relation Return value of attribute options.
	 *
	 * @param option the new option
	 * @return <code>Option</code>
	 */
	public void setOption(final Option[] option) {
		this.option = option;
	}

	/**
	 * Relation Set value of attribute options.
	 *
	 * @return the option
	 */
	public Option[] getOption() {
		return this.option;
	}
}
