/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.catalog.option;



/**
 * The Class Alarm.
 */
public class Alarm implements Option {
	/**
	 * {@inheritDoc}
	 */
	public long getPrice() {
		return 2000;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getDescription() {
		return "Alarm";
	}

	/**
	 * {@inheritDoc}
	 */
	public String getIdentifier() {
		return "alarm";
	}
}
