/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.catalog.engine;


/**
 * The Class Engine.
 */
public abstract class Engine {
	
	/**
	 * Gets the price.
	 *
	 * @return <code>long</code> :
	 */
	public abstract long getPrice();
}
