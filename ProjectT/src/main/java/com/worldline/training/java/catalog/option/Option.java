/* Copyright (C) Atos Worldline
 $Id$
 $Log$*/
package com.worldline.training.java.catalog.option;


/**
 * The Interface Option.
 */
public interface Option {
	
	/**
	 * Gets the price.
	 *
	 * @return <code>long</code> :
	 */
	public long getPrice();

	/**
	 * Gets the description.
	 *
	 * @return <code>String</code> :
	 */
	public String getDescription();

	/**
	 * Gets the identifier.
	 *
	 * @return <code>String</code> :
	 */
	public String getIdentifier();
}
